" function: install vim-plug
function KVim_Install()
	" install fonts:   
	" 	1. download font *.zip from http://nerdfonts.com/
	" 	2. extract into ~/.fonts
	" 	3. execute "sudo fc-cache -fv"
	" install ctags:
	" 	1. execute "sudo apt install autoconf"
	" 	2. install "https://ctags.io/"
	exec ':! sudo apt install fonts-powerline'
	exec ':! sudo apt install git'
	exec ':! curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
	call KVim_Plugins()
	PlugInstall
endfunction

" function: init vim plugins
function KVim_Plugins()
	call plug#begin('~/.vim/plugged')
	Plug 'rdavison/Libertine'
	Plug 'scrooloose/nerdtree'
	Plug 'scrooloose/nerdcommenter'
	Plug 'jistr/vim-nerdtree-tabs'
	Plug 'vim-airline/vim-airline'
	Plug 'vim-airline/vim-airline-themes'
	Plug 'Lokaltog/vim-powerline'
	Plug 'ryanoasis/vim-devicons'
	Plug 'vim-scripts/OmniCppComplete'
	Plug 'ervandew/supertab'
	Plug 'vim-erlang/vim-erlang-omnicomplete'
	Plug 'majutsushi/tagbar'
	Plug 'jiangmiao/auto-pairs'
	Plug 'alvan/vim-closetag'


	Plug 'vim-syntastic/syntastic'
	Plug 'tpope/vim-pathogen'
	Plug 'tpope/vim-surround'
	Plug 'tpope/vim-fugitive'
	call plug#end() 
endfunction

" function init vim parameters
function KVim_Init()
	" init: vim      
	" map:
	" 	cf: code format
	" 	co: code folds open
	" 	cc: code folds close
	" 	cd: code directories
	" 	ct: code tags
	" 	cs: code shell
	set laststatus=2
	set encoding=UTF-8
	set t_Co=256
	set mouse=a
	set number
	set nocp
	set ic
	set hls
	set foldmethod=indent
	set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab
	set autoindent 
	set noexpandtab 
	set tabstop=4 
	set shiftwidth=4
	set autoindent 
	set smartindent
	set nowrap
	set cindent
	execute pathogen#infect()
	syntax on
	filetype plugin indent on
	map <space> za
	map cf G=gg
	map co zR
	map cc zM
	map cd :NERDTreeToggle<CR>
	map ct :TagbarToggle<CR>
	augroup autoformat
		autocmd BufEnter all cf
	augroup END

	" init: select and copy
	" map:
	" 	Shift-{Up|Left|Down|Right}: select
	" 	Ctrl-c: copy
	" 	Ctrl-x: cut
	" 	Ctrl-v: paste
	nmap <S-Up> v<Up>
	nmap <S-Down> v<Down>
	nmap <S-Left> v<Left>
	nmap <S-Right> v<Right>
	vmap <S-Up> <Up>
	vmap <S-Down> <Down>
	vmap <S-Left> <Left>
	vmap <S-Right> <Right>
	imap <S-Up> <Esc>v<Up>
	imap <S-Down> <Esc>v<Down>
	imap <S-Left> <Esc>v<Left>
	imap <S-Right> <Esc>v<Right>
	vmap <C-c> y<Esc>i
	vmap <C-x> d<Esc>i
	map <C-v> pi
	imap <C-v> <Esc>pi
	imap <C-z> <Esc>ui

	" init: gruvbox
	" let g:gruvbox_contrast_dark='hard'
	" let g:libertine_Pitch = 1
	let g:libertine_Midnight = 1
	colorscheme libertine
	" set background=dark

	" init: nerdtree
	" map:
	" 	t{Right|Left}: tab move
	" 	t{Down}: tab delete
	augroup nerdtree
		autocmd vimenter * NERDTree
		autocmd StdinReadPre * let s:std_in=1
		autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif
		autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
	augroup END
	let g:NERDTreeDirArrowExpandable = '▸'
	let g:NERDTreeDirArrowCollapsible = '▾'
	" let g:NERDTreeMapOpenInTab='<2-LeftMouse>'
	" let g:NERDTreeMapOpenInTab='<ENTER>'
	" let g:NERDTreeMouseMode=3
	let g:NERDSpaceDelims=1
	let g:NERDCompactSexyComs=1
	let g:NERDDefaultAlign='left'
	let g:NERDAltDelims_java=1
	let g:NERDCustomDelimiters={ 'c': { 'left': '/**','right': '*/' } }
	let g:NERDCommentEmptyLines=1
	let g:NERDTrimTrailingWhitespace=1
	if has('win32')
		nmap <C-/> <leader>c<Space>
		vmap <C-/> <leader>c<Space>
	else
		nmap <C-_> <leader>c<Space>
		vmap <C-_> <leader>c<Space>
	endif
	let g:nerdtree_tabs_open_on_console_startup=1
	map t<Right> :bnext<cr>
	map t<Left> :bprev<cr>
	nnoremap t<Down> :bp\|bd #<cr>

	" init: airline
	let g:airline#extensions#tabline#enabled = 1
	let g:airline#extensions#tabline#fnamemod = ':t'
	let g:airline#extensions#tabline#switch_buffers_and_tabs = 1
	let g:airline#extensions#tabline#buffer_idx_mode = 1
	let g:airline_left_sep = "\uE0B4"
	let g:airline_right_sep = "\uE0B6"
	let g:airline_section_z = airline#section#create(["\uE0A1" . '%{line(".")}' . "\uE0A3" . '%{col(".")}'])
	let g:airline#extensions#tabline#enabled = 1
	let g:airline#extensions#tabline#left_sep = ' '
	let g:airline#extensions#tabline#left_alt_sep = '|'
	let g:airline#extensions#tabline#formatter = 'unique_tail'
	let g:airline_powerline_fonts=1
	let g:airline_theme='wombat'
	let g:airline_solarized_bg='dark'
	let g:Powerline_symbols='unicode'

	" init: autocomplete
	set completeopt=longest,menuone
	augroup autocomplete
		autocmd BufNewFile ,BufRead,BufEnter css setlocal omnifunc=csscomplete#CompleteCSS
		autocmd BufNewFile,BufRead,BufEnter html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
		autocmd BufNewFile,BufRead,BufEnter javascript setlocal omnifunc=javascriptcomplete#CompleteJS
		autocmd BufNewFile,BufRead,BufEnter python setlocal omnifunc=pythoncomplete#Complete
		autocmd BufNewFile,BufRead,BufEnter php setlocal omnifunc=phpcomplete#CompletePHP
		autocmd BufNewFile,BufRead,BufEnter erlang setlocal omnifunc=erlang_complete#Complete
		autocmd BufNewFile,BufRead,BufEnter python setlocal omnifunc=pythoncomplete#Complete
		autocmd BufNewFile,BufRead,BufEnter xml setlocal omnifunc=xmlcomplete#CompleteTags
		" autocmd BufNewFile,BufRead,BufEnter *.c,*.h setlocal omnifunc=omni#c#complete#Main
		" autocmd BufNewFile,BufRead,BufEnter *.cpp,*.hpp setlocal omnifunc=omni#cpp#complete#Main
	augroup END
	inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
	inoremap <expr> <C-n> pumvisible() ? '<C-n>' :
				\ '<C-n><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'

	inoremap <expr> <M-,> pumvisible() ? '<C-n>' :
				\ '<C-x><C-o><C-n><C-p><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'
	" open omni completion menu closing previous if open and opening new menu without changing the text
	inoremap <expr> <C-@> (pumvisible() ? (col('.') > 1 ? '<Esc>i<Right>' : '<Esc>i') : '') .
				\ '<C-x><C-o><C-r>=pumvisible() ? "\<lt>C-n>\<lt>C-p>\<lt>Down>" : ""<CR>'
	" open user completion menu closing previous if open and opening new menu without changing the text
	inoremap <expr> <S-@> (pumvisible() ? (col('.') > 1 ? '<Esc>i<Right>' : '<Esc>i') : '') .
				\ '<C-x><C-u><C-r>=pumvisible() ? "\<lt>C-n>\<lt>C-p>\<lt>Down>" : ""<CR>'

	" init: syntastic
	set statusline+=%#warningmsg#
	set statusline+=%{SyntasticStatuslineFlag()}
	set statusline+=%*

	let g:syntastic_always_populate_loc_list = 1
	let g:syntastic_auto_loc_list = 1
	let g:syntastic_check_on_open = 1
	let g:syntastic_check_on_wq = 0
endfunction

" function: create new project
function KVim_New()  
	echo 'Creating  Project based on KPS Project Standard !'
	let name=input('Enter Project name: ')
	let git=input('Enter Project git remote repository link : ')
	exec ':! sudo apt install python-pip' 
	exec ':! pip install mkdocs'
	exec ':! pip install mkdocs-material'
	exec ':!mkdir '.l:name
	exec ':!mkdir '.l:name.'/docs'
	exec ':!mkdir '.l:name.'/source'
	exec ':!mkdocs new '.l:name.'/docs'
	echo 'Project created !'
endfunction 

call KVim_Plugins()
call KVim_Init()

